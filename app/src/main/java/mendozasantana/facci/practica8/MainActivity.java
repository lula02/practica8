package mendozasantana.facci.practica8;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    TextView cajaCedula, cajaNombre, cajaApellido, cajaDatos;
    Button botonLeer, botonEscribir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        cajaCedula = (TextView) findViewById(R.id.TXTcedulaMI);
        cajaNombre = (TextView) findViewById(R.id.TXTnombreMI);
        cajaApellido = (TextView) findViewById(R.id.TXTapellidosMI);
        cajaDatos = (TextView) findViewById(R.id.TXTdatosMI);

        botonEscribir = (Button) findViewById(R.id.BTNescribirMI);
        botonLeer = (Button) findViewById(R.id.BTNleerMI);

        botonEscribir.setOnClickListener(this);
        botonLeer.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.BTNescribirMI:
                try {
                    OutputStreamWriter escritor = new OutputStreamWriter
                            (openFileOutput("archivo.txt", Context.MODE_APPEND));
                    escritor.write(cajaCedula.getText().toString() + "," + cajaApellido.getText().toString() + ","
                            + cajaNombre.getText().toString() + "," + cajaDatos.getText().toString() + ",");
                    escritor.close();
                }catch (Exception ex){
                    Log.e("Archivo MI", "Error en el archivo de escritura");
                }
                break;
            case R.id.BTNleerMI:
                try {
                    BufferedReader lector = new BufferedReader(new InputStreamReader(openFileInput("archivo.txt")));
                    String datos = lector.readLine();
                    String[] listaPersonas = datos.split(";");
                    for (int i = 0; i < listaPersonas.length; i++) {
                        cajaDatos.append(listaPersonas[i].split(",")[0] + " " + listaPersonas[i].split(",")[1] + " " + listaPersonas[i].split(",")[2]+"");
                    }
                   // cajaDatos.setText(" ");

                    lector.close();
                }catch (Exception ex){
                    Log.e("Archivo Mi", "Error en la lectura del archivo"+
                    ex.getMessage());
                }
                break;
        }

    }
}

